<header class="header3">

  <div class="logo-infos">
    
      <!-- BOTTOM BAR -->
      <div class="container">

        <?php $add_place_banner = ''; ?>
        <?php if ( function_exists('evdevtheme_framework')) { ?>
          <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
            <?php if (function_exists('evdev_extend_redux_tabs')){ ?>
              <?php if (evdev_redux('mt_header_ad_banner_status')) { ?>
                <?php if(evdev_redux('mt_header_ad_place_banner','url') != '' && evdev_redux('mt_header_ad_banner_status') == 'on'){ ?>
                    <?php $add_place_banner = 'add_place_banner'; ?>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>

        <div class="row logo-infos-row <?php echo esc_attr($add_place_banner); ?>">

          <!-- LOGO HOLDER -->
          <div class="navbar-header col-md-3">
            <?php 

            $custom_header_activated = get_post_meta( get_the_ID(), 'smartowl_custom_header_options_status', true );
            $header_v = get_post_meta( get_the_ID(), 'smartowl_header_custom_variant', true );
            $custom_logo_url = get_post_meta( get_the_ID(), 'smartowl_header_custom_logo', true );

            if($custom_header_activated == 'yes' && isset($custom_logo_url) && !empty($custom_logo_url)) { ?>

              <h1 class="logo">
                  <a href="<?php echo esc_url(get_site_url()); ?>">
                      <img src="<?php echo esc_url($custom_logo_url); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                  </a>
              </h1>

            <?php } else {

              if(evdev_redux('mt_logo','url')){ ?>
                <h1 class="logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                        <img src="<?php echo esc_url(evdev_redux('mt_logo','url')); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                    </a>
                </h1>
              <?php }else{ ?>
                <h1 class="logo no-logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                      <?php echo esc_html(get_bloginfo()); ?>
                    </a>
                </h1>
              <?php } ?>
            <?php } ?>

            <!-- NAVIGATION BURGER MENU -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          </div>

          <!-- RIGHT SIDE HOLDER -->
          <div class="col-md-9 social-links-column">
            <div class="add-banner-header">
            <?php if ( function_exists('evdevtheme_framework')) { ?>
              <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
                <?php if (function_exists('evdev_extend_redux_tabs')){ ?>
                  <?php if (evdev_redux('mt_header_ad_banner_status')) {
                    if(evdev_redux('mt_header_single_adsense_code') != '' && evdev_redux('mt_header_ad_banner_status') == 'on_adsense'){
                      echo evdev_redux('mt_header_single_adsense_code');
                    }elseif(evdev_redux('mt_header_ad_place_banner','url') != '' && evdev_redux('mt_header_ad_banner_status') == 'on'){ ?>
                        <a href="<?php echo esc_url(evdev_redux('mt_header_ad_place_banner_link')) ?>" target="_blank">
                            <img src="<?php echo esc_url(evdev_redux('mt_header_ad_place_banner','url')) ?>" alt="<?php echo esc_attr__('adPlace','evdev'); ?>" />
                        </a>
                    <?php }
                  }
                }
              } 
            }?>

          </div>


            <ul class="social-links social-links-labels text-right">
              <?php if ( evdev_redux('mt_social_fb') && evdev_redux('mt_social_fb') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_fb') ) ?>"><i class="fa fa-facebook"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_tw') && evdev_redux('mt_social_tw') != '' ) { ?>
                  <li><a href="https://twitter.com/<?php echo esc_attr( evdev_redux('mt_social_tw') ) ?>"><i class="fa fa-twitter"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_gplus') && evdev_redux('mt_social_gplus') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_gplus') ) ?>"><i class="fa fa-google-plus"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_youtube') && evdev_redux('mt_social_youtube') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_youtube') ) ?>"><i class="fa fa-youtube"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_pinterest') && evdev_redux('mt_social_pinterest') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_pinterest') ) ?>"><i class="fa fa-pinterest"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_linkedin') && evdev_redux('mt_social_linkedin') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_linkedin') ) ?>"><i class="fa fa-linkedin"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_skype') && evdev_redux('mt_social_skype') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_skype') ) ?>"><i class="fa fa-skype"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_instagram') && evdev_redux('mt_social_instagram') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_instagram') ) ?>"><i class="fa fa-instagram"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_dribbble') && evdev_redux('mt_social_dribbble') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_dribbble') ) ?>"><i class="fa fa-dribbble"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_deviantart') && evdev_redux('mt_social_deviantart') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_deviantart') ) ?>"><i class="fa fa-deviantart"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_digg') && evdev_redux('mt_social_digg') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_digg') ) ?>"><i class="fa fa-digg"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_flickr') && evdev_redux('mt_social_flickr') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_flickr') ) ?>"><i class="fa fa-flickr"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_stumbleupon') && evdev_redux('mt_social_stumbleupon') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_stumbleupon') ) ?>"><i class="fa fa-stumbleupon"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_tumblr') && evdev_redux('mt_social_tumblr') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_tumblr') ) ?>"><i class="fa fa-tumblr"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_vimeo') && evdev_redux('mt_social_vimeo') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_vimeo') ) ?>"><i class="fa fa-vimeo-square"></i></a></li>
              <?php } ?>
              <?php if ( evdev_redux('mt_social_telegram') && evdev_redux('mt_social_telegram') != '' ) { ?>
                  <li><a href="<?php echo esc_url( evdev_redux('mt_social_telegram') ) ?>"><i class="fa fa-telegram"></i></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
   
  </div>



  <!-- BOTTOM BAR -->
  <nav class="navbar navbar-default" id="evdevtheme-main-head">
    <div class="container">
      <div class="row evdevtheme-main-head-row">
        <!-- NAV MENU -->
        <div class="col-md-1 right-side-social-actions">
          <?php if(evdev_redux('mt_header_fixed_sidebar_menu_status') == true) { ?>
              <!-- ACTIONS BUTTONS GROUP -->
              <div class="actions-group">          
                  <!-- MT BURGER -->
                  <div id="mt-nav-burger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                  </div>           
              </div>
          <?php } ?>
        </div>

        <?php $nav_class = 'col-md-10';
        if( !class_exists( 'ReduxFrameworkPlugin' ) || evdev_redux('mt_header_is_search') == true && evdev_redux('mt_header_fixed_sidebar_menu_status') == true) {
            $nav_class = 'col-md-10';
        } else {
            $nav_class = 'col-md-10';
        } ?>

        <div id="navbar" class="navbar-collapse collapse <?php echo esc_attr($nav_class); ?>">
          <ul class="menu nav navbar-nav nav-effect nav-menu">
            <?php
              if ( has_nav_menu( 'primary' ) ) {
                $defaults = array(
                  'menu'            => '',
                  'container'       => false,
                  'container_class' => '',
                  'container_id'    => '',
                  'menu_class'      => 'menu',
                  'menu_id'         => '',
                  'echo'            => true,
                  'fallback_cb'     => false,
                  'before'          => '',
                  'after'           => '',
                  'link_before'     => '',
                  'link_after'      => '',
                  'items_wrap'      => '%3$s',
                  'depth'           => 0,
                  'walker'          => ''
                );

                $defaults['theme_location'] = 'primary';

                wp_nav_menu( $defaults );
              }else{
                echo '<p class="no-menu text-right">';
                  echo esc_html__('Primary navigation menu is missing. Add one from ', 'evdev');
                  echo '<a href="'.esc_url(get_admin_url() . 'nav-menus.php').'"><strong>'.esc_html__(' Appearance -> Menus','evdev').'</strong></a>';
                echo '</p>';
              }
            ?>
          </ul>
        </div>

        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
          <?php if(evdev_redux('mt_header_is_search') == true){ ?>
            <div class="header-nav-actions col-md-1 text-right">
                <a href="<?php echo esc_url('#'); ?>" class="mt-search-icon">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            </div>
          <?php } ?>
        <?php } else { ?>
          <div class="header-nav-actions col-md-1 text-right">
              <a href="<?php echo esc_url('#'); ?>" class="mt-search-icon">
                <i class="fa fa-search" aria-hidden="true"></i>
              </a>
          </div>
        <?php } ?>

      </div>
    </div>
  </nav>
</header>