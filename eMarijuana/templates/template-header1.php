<header class="header1">

  <?php if (evdev_redux('mt_header_top_bar_status') == true) { ?>
  <div class="row top-header">

	    <div class="col-md-8 col-sm-7">
	    	<div class="header-infos">
			    <?php if(evdev_redux('mt_divider_header_info_1_status') == true){ ?>
			      <!-- HEADER INFO 1 -->
			      <div class="text-center header-info-group">
			        <div class="header-info-icon pull-left text-center <?php echo esc_attr(evdev_redux('mt_divider_header_info_1_media_type')); ?>">
			          <?php if(evdev_redux('mt_divider_header_info_1_media_type') == 'font_awesome'){ ?>
			            <i class="<?php echo esc_attr(evdev_redux('mt_divider_header_info_1_faicon')); ?>"></i>
			          <?php }elseif(evdev_redux('mt_divider_header_info_1_media_type') == 'media_image'){ ?>
			            <img src="<?php echo esc_url(evdev_redux('mt_divider_header_info_1_image_icon','url')); ?>" alt="<?php echo esc_attr__('Image Header Info', 'evdev'); ?>" />
			          <?php }elseif(evdev_redux('mt_divider_header_info_1_media_type') == 'text_title'){ ?>
			          	<p class="header_text_title"><?php echo esc_html(evdev_redux('mt_divider_header_info_1_text_1')); ?>
			          <?php } ?>
			        </div>
			        <div class="header-info-labels pull-left">
			          <p><?php echo esc_html(evdev_redux('mt_divider_header_info_1_heading1')); ?></p>
			        </div>
			      </div>
			      <!-- // HEADER INFO 1 -->
			    <?php } ?>

			    <?php if(evdev_redux('mt_divider_header_info_2_status') == true){ ?>
			      <!-- HEADER INFO 2 -->
			      <div class="text-center header-info-group">
			        <div class="header-info-icon pull-left text-center <?php echo esc_attr(evdev_redux('mt_divider_header_info_2_media_type')); ?>">
			          <?php if(evdev_redux('mt_divider_header_info_2_media_type') == 'font_awesome'){ ?>
			            <i class="<?php echo esc_attr(evdev_redux('mt_divider_header_info_2_faicon')); ?>"></i>
			          <?php }elseif(evdev_redux('mt_divider_header_info_2_media_type') == 'media_image'){ ?>
			            <img src="<?php echo esc_url(evdev_redux('mt_divider_header_info_2_image_icon','url')); ?>" alt="<?php echo esc_attr__('Image Header Info', 'evdev'); ?>" />
			          <?php }elseif(evdev_redux('mt_divider_header_info_2_media_type') == 'text_title'){ ?>
			          	<p class="header_text_title"><?php echo esc_html(evdev_redux('mt_divider_header_info_2_text_2')); ?>
			          <?php } ?>
			        </div>
			        <div class="header-info-labels pull-left">
			          <p><?php echo esc_html(evdev_redux('mt_divider_header_info_2_heading1')); ?></p>
			        </div>
			      </div>
			      <!-- // HEADER INFO 2 -->
			    <?php } ?>

			    <?php if(evdev_redux('mt_divider_header_info_3_status') == true){ ?>
			      <!-- HEADER INFO 3 -->
			      <div class="text-center header-info-group">
			        <div class="header-info-icon pull-left text-center <?php echo esc_attr(evdev_redux('mt_divider_header_info_3_media_type')); ?>">
			          <?php if(evdev_redux('mt_divider_header_info_3_media_type') == 'font_awesome'){ ?>
			            <i class="<?php echo esc_attr(evdev_redux('mt_divider_header_info_3_faicon')); ?>"></i>
			          <?php }elseif(evdev_redux('mt_divider_header_info_3_media_type') == 'media_image'){ ?>
			            <img src="<?php echo esc_url(evdev_redux('mt_divider_header_info_3_image_icon','url')); ?>" alt="<?php echo esc_attr__('Image Header Info', 'evdev'); ?>" />
			          <?php }elseif(evdev_redux('mt_divider_header_info_3_media_type') == 'text_title'){ ?>
			          	<p class="header_text_title"><?php echo esc_html(evdev_redux('mt_divider_header_info_3_text_3')); ?>
			          <?php } ?>
			        </div>
			        <div class="header-info-labels pull-left">
			          <p class="call_us_class"><?php echo esc_html(evdev_redux('mt_divider_header_info_3_heading1')); ?></p>
			        </div>
			      </div>
			      <!-- // HEADER INFO 3 -->
			    <?php } ?>

				</div>
		</div>

	      <!-- NAV ACTIONS -->
            <div class="navbar-collapse actions collapse col-md-4  col-sm-5">
                <div class="header-nav-actions">

                  <?php if(evdev_redux('mt_header_fixed_sidebar_menu_status') == true) { ?>
                    <!-- MT BURGER -->
                    <div id="mt-nav-burger">
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                  <?php } ?>

                  <?php if(evdev_redux('mt_header_is_search') == true){ ?>
                    <a href="<?php echo esc_url('#'); ?>" class="mt-search-icon">
                      <i class="fa fa-search" aria-hidden="true"></i>
                    </a>
                  <?php } ?>

                  <ul class="social-links">
                        <?php if ( evdev_redux('mt_social_fb') && evdev_redux('mt_social_fb') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_fb') ) ?>"><i class="fa fa-facebook"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_tw') && evdev_redux('mt_social_tw') != '' ) { ?>
                            <li><a href="https://twitter.com/<?php echo esc_attr( evdev_redux('mt_social_tw') ) ?>"><i class="fa fa-twitter"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_gplus') && evdev_redux('mt_social_gplus') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_gplus') ) ?>"><i class="fa fa-google-plus"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_youtube') && evdev_redux('mt_social_youtube') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_youtube') ) ?>"><i class="fa fa-youtube"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_pinterest') && evdev_redux('mt_social_pinterest') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_pinterest') ) ?>"><i class="fa fa-pinterest"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_linkedin') && evdev_redux('mt_social_linkedin') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_linkedin') ) ?>"><i class="fa fa-linkedin"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_skype') && evdev_redux('mt_social_skype') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_skype') ) ?>"><i class="fa fa-skype"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_instagram') && evdev_redux('mt_social_instagram') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_instagram') ) ?>"><i class="fa fa-instagram"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_dribbble') && evdev_redux('mt_social_dribbble') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_dribbble') ) ?>"><i class="fa fa-dribbble"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_deviantart') && evdev_redux('mt_social_deviantart') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_deviantart') ) ?>"><i class="fa fa-deviantart"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_digg') && evdev_redux('mt_social_digg') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_digg') ) ?>"><i class="fa fa-digg"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_flickr') && evdev_redux('mt_social_flickr') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_flickr') ) ?>"><i class="fa fa-flickr"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_stumbleupon') && evdev_redux('mt_social_stumbleupon') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_stumbleupon') ) ?>"><i class="fa fa-stumbleupon"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_tumblr') && evdev_redux('mt_social_tumblr') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_tumblr') ) ?>"><i class="fa fa-tumblr"></i></a></li>
                        <?php } ?>
                        <?php if ( evdev_redux('mt_social_vimeo') && evdev_redux('mt_social_vimeo') != '' ) { ?>
                            <li><a href="<?php echo esc_url( evdev_redux('mt_social_vimeo') ) ?>"><i class="fa fa-vimeo-square"></i></a></li>
                        <?php } ?>
                    </ul>



                </div>
                
            </div>

   </div>
  <?php } ?>


  <!-- BOTTOM BAR -->
  <nav class="navbar navbar-default logo-infos" id="evdevtheme-main-head">
        <div class="row_wrapper">

          <!-- LOGO -->
          <div class="navbar-header">
            <!-- NAVIGATION BURGER MENU -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>



            <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { 
              
            $custom_header_activated = get_post_meta( get_the_ID(), 'smartowl_custom_header_options_status', true );
            $header_v = get_post_meta( get_the_ID(), 'smartowl_header_custom_variant', true );
            $custom_logo_url = get_post_meta( get_the_ID(), 'smartowl_header_custom_logo', true );

            if($custom_header_activated == 'yes' && isset($custom_logo_url) && !empty($custom_logo_url)) { ?>

              <h1 class="logo">
                  <a href="<?php echo esc_url(get_site_url()); ?>">
                      <img src="<?php echo esc_url($custom_logo_url); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                  </a>
              </h1>

            <?php } else {

              if(evdev_redux('mt_logo','url')){ ?>
                <h1 class="logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                        <img src="<?php echo esc_url(evdev_redux('mt_logo','url')); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                    </a>
                </h1>
              <?php }else{ ?>
                <h1 class="logo no-logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                      <?php echo esc_html(get_bloginfo()); ?>
                    </a>
                </h1>
              <?php } ?>
            <?php } ?>

            <?php }else{ ?>
              <h1 class="logo no-logo">
                  <a href="<?php echo esc_url(get_site_url()); ?>">
                    <?php echo esc_html(get_bloginfo()); ?>
                  </a>
              </h1>
            <?php } ?>
          </div>

          <!-- NAV MENU -->
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="menu nav navbar-nav pull-none nav-effect nav-menu">
              <?php
                if ( has_nav_menu( 'primary' ) ) {
                  $defaults = array(
                    'menu'            => '',
                    'container'       => false,
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => false,
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '%3$s',
                    'depth'           => 0,
                    'walker'          => ''                    
                  );

                  $defaults['theme_location'] = 'primary';
                  //if( !is_page( 'failed-age-veri' ) )
                  global $post;
                  $age_option = get_option( 'psag_options' )['psag_options_redirect_url'];
                  if( isset( $age_option ) && $post->post_name != end( explode( '/', $age_option ) ) ) {
                    wp_nav_menu( $defaults );
                  }
                  
                }else{
                  echo '<p class="no-menu text-left">';
                    echo esc_html__('Primary navigation menu is missing. Add one from ', 'evdev');
                    echo '<a href="'.esc_url(get_admin_url() . 'nav-menus.php').'"><strong>'.esc_html__(' Appearance -> Menus','evdev').'</strong></a>';
                  echo '</p>';
                }
              ?>

              <?php if ( function_exists('evdevtheme_framework')) { ?>
                
                <?php 
                $mt_request_button_text = 'REQUEST AN APPOINTMENT';
                if (evdev_redux('mt_request_button_text')) {
                  $mt_request_button_text = evdev_redux('mt_request_button_text');
                }
                $mt_request_button = '#';
                if (evdev_redux('mt_request_button')) {
                  $mt_request_button = evdev_redux('mt_request_button');
                }
                ?>
                <li id="nav-menu-list" class="nav-menu-button"><a href="<?php echo esc_url( $mt_request_button ); ?>"><?php echo esc_html($mt_request_button_text); ?></a></li>
              <?php } ?>
                <li class="cart 12">
                  <a href="<?php echo WC()->cart->get_cart_url(); ?>" class="cart-contents">
                  <span class="cart_contents_count"><?php echo WC()->cart->cart_contents_count; ?></span>
                  </a>
                </li>
              <div class="pull-right actions-group">
                <?php if(evdev_redux('mt_header_fixed_sidebar_menu_status') == true) { ?>
                  <!-- MT BURGER -->
                  <div class="nav-burger">
                    <div id="mt-nav-burger">
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                  </div>
                <?php } ?>

                <?php if(evdev_redux('mt_header_is_search') == true){ ?>
                  <!-- SEARCH ICON -->
                  <div class="nav-search">
                    <a href="<?php echo esc_url('#'); ?>" class="mt-search-icon">
                      <i class="fa fa-search" aria-hidden="true"></i>
                    </a>
                  </div>
                <?php } ?>
              </div>

              <?php if ( class_exists('woocommerce')) { ?>
                <?php if (is_user_logged_in()) { ?> 
                  <div id="dropdown-user-profile" class="ddmenu">
                      <li id="nav-menu-register" class="nav-menu-account"><i class="fa fa-user" aria-hidden="true"></i></li>
                    <ul>
                      <li><a href="<?php echo esc_url(get_permalink( get_option('woocommerce_myaccount_page_id') )); ?>"><i class="icon-layers icons"></i> <?php echo esc_html__('My Dashboard','evdev'); ?></a></li>
                      <li><a href="<?php echo esc_url(get_permalink(get_option('woocommerce_myaccount_page_id')).'orders'); ?>"><i class="icon-bag icons"></i> <?php echo esc_html__('My Orders','evdev'); ?></a></li>
                      <li><a href="<?php echo esc_url(get_permalink(get_option('woocommerce_myaccount_page_id')).'edit-account'); ?>"><i class="icon-user icons"></i> <?php echo esc_html__('Account Details','evdev'); ?></a></li>
                      <div class="dropdown-divider"></div>
                      <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="icon-logout icons"></i> <?php echo esc_html__('Log Out','evdev'); ?></a></li>
                    </ul>
                  </div>

                <?php } else { ?>
                  <li id="nav-menu-login" class="evdev-logoin">
                    <a href="<?php echo esc_url(get_permalink( get_option('woocommerce_myaccount_page_id') )); ?>">Create Account</a>
                  </li>
                <?php } ?>
              <?php } ?>

            </ul>

          </div>

        </div>
  </nav>
</header>
