<?php
if ( ! isset( $content_width ) ) {
    $content_width = 640; /* pixels */
}


/**
||-> evdev_redux
*/
function evdev_redux($redux_meta_name1 = '',$redux_meta_name2 = ''){

    global  $evdev_redux;

    $html = '';
    if (isset($redux_meta_name1) && !empty($redux_meta_name2)) {
        $html = $evdev_redux[$redux_meta_name1][$redux_meta_name2];
    }elseif(isset($redux_meta_name1) && empty($redux_meta_name2)){
        $html = $evdev_redux[$redux_meta_name1];
    }
    
    return $html;

}


/**
||-> evdev_setup
*/
function evdev_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on evdev, use a find and replace
     * to change 'evdev' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'evdev', get_template_directory() . '/languages' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary menu', 'evdev' ),
    ) );

    // ADD THEME SUPPORT
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) );
    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
    // Enable support for Post Formats.
    add_theme_support( 'custom-background', apply_filters( 'smartowl_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );// Set up the WP core custom background feature.
    add_theme_support( 'wp-block-styles' );

}
add_action( 'after_setup_theme', 'evdev_setup' );


/**
||-> Register widget areas.
*/
function evdev_widgets_init() {

    global  $evdev_redux;

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'evdev' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Main Theme Sidebar', 'evdev' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Products Filter', 'evdev' ),
        'id'            => 'sidebar-products-filter',
        'description'   => esc_html__( 'Products Filter Sidebar', 'evdev' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    
    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        if (!empty($evdev_redux['mt_dynamic_sidebars'])){
            foreach ($evdev_redux['mt_dynamic_sidebars'] as &$value) {
                $id           = str_replace(' ', '', $value);
                $id_lowercase = strtolower($id);
                if ($id_lowercase) {
                    register_sidebar( array(
                        'name'          => esc_html($value),
                        'id'            => esc_html($id_lowercase),
                        'description'   => esc_html__( 'Sidebar ', 'evdev' ) . esc_html($value),
                        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</aside>',
                        'before_title'  => '<h3 class="widget-title">',
                        'after_title'   => '</h3>',
                    ) );
                }
            }
        }
    }
    
    // FOOTER ROW 1
    if (isset($evdev_redux['mt_footer_row_1'])) {
        switch ($evdev_redux['mt_footer_row_1']) {
            case '0':
                break;
            case '1':
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    if (isset($evdev_redux['mt_footer_row_1_layout'])) {
                        $footer_row_1 = $evdev_redux['mt_footer_row_1_layout'];
                        $nr1 = array("1", "2", "3", "4", "5", "6");
                        if (in_array($footer_row_1, $nr1)) {
                            for ($i=1; $i <= $footer_row_1 ; $i++) { 
                                register_sidebar( array(
                                    'name'          => esc_html__( 'Footer Row 1 - Sidebar ','evdev').esc_html($i),
                                    'id'            => 'footer_row_1_'.esc_html($i),
                                    'description'   => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                    'after_widget'  => '</aside>',
                                    'before_title'  => '<h3 class="widget-title">',
                                    'after_title'   => '</h3>',
                                ) );
                            }
                        }elseif ($footer_row_1 == 'column_half_sub_half' || $footer_row_1 == 'column_sub_half_half') {
                            $footer_row_1 = '3';
                            for ($i=1; $i <= $footer_row_1 ; $i++) { 
                                register_sidebar( array(
                                    'name'          => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'id'            => 'footer_row_1_'.esc_html($i),
                                    'description'   => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                    'after_widget'  => '</aside>',
                                    'before_title'  => '<h3 class="widget-title">',
                                    'after_title'   => '</h3>',
                                ) );
                            }
                        }elseif ($footer_row_1 == 'column_sub_fourth_third' || $footer_row_1 == 'column_third_sub_fourth') {
                            $footer_row_1 = '5';
                            for ($i=1; $i <= $footer_row_1 ; $i++) { 
                                register_sidebar( array(
                                    'name'          => esc_html__( 'Footer Row 1 - Sidebar ','evdev').esc_html($i),
                                    'id'            => 'footer_row_1_'.esc_html($i),
                                    'description'   => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                    'after_widget'  => '</aside>',
                                    'before_title'  => '<h3 class="widget-title">',
                                    'after_title'   => '</h3>',
                                ) );
                            }
                        }elseif ($footer_row_1 == 'column_sub_third_half' || $footer_row_1 == 'column_half_sub_third' || $footer_row_1 == 'column_5_2_2_3') {
                            $footer_row_1 = '4';
                            for ($i=1; $i <= $footer_row_1 ; $i++) { 
                                register_sidebar( array(
                                    'name'          => esc_html__( 'Footer Row 1 - Sidebar ','evdev').esc_html($i),
                                    'id'            => 'footer_row_1_'.esc_html($i),
                                    'description'   => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                    'after_widget'  => '</aside>',
                                    'before_title'  => '<h3 class="widget-title">',
                                    'after_title'   => '</h3>',
                                ) );
                            }
                        }elseif ($footer_row_1 == 'column_2_10') {
                            $footer_row_1 = '2';
                            for ($i=1; $i <= $footer_row_1 ; $i++) { 
                                register_sidebar( array(
                                    'name'          => esc_html__( 'Footer Row 1 - Sidebar ','evdev').esc_html($i),
                                    'id'            => 'footer_row_1_'.esc_html($i),
                                    'description'   => esc_html__( 'Footer Row 1 - Sidebar ', 'evdev' ) . esc_html($i),
                                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                    'after_widget'  => '</aside>',
                                    'before_title'  => '<h3 class="widget-title">',
                                    'after_title'   => '</h3>',
                                ) );
                            }
                        }
                    }
                break;
                }
        }
    }


    // FOOTER ROW 2
    if (isset($evdev_redux['mt_footer_row_2'])) {
        switch ($evdev_redux['mt_footer_row_2']) {
            case '0':
                break;
            case '1':
                if (isset($evdev_redux['mt_footer_row_2_layout'])) {
                    $footer_row_2 = $evdev_redux['mt_footer_row_2_layout'];
                    $nr2 = array("1", "2", "3", "4", "5", "6");
                    if (in_array($footer_row_2, $nr2)) {
                        for ($i=1; $i <= $footer_row_2 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev').esc_html($i),
                                'id'            => 'footer_row_2_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_2 == 'column_half_sub_half' || $footer_row_2 == 'column_sub_half_half') {
                        $footer_row_2 = '2';
                        for ($i=1; $i <= $footer_row_2 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 2 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_2_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_2 == 'column_sub_fourth_third' || $footer_row_2 == 'column_third_sub_fourth') {
                        $footer_row_2 = '5';
                        for ($i=1; $i <= $footer_row_2 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 2 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_2_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_2 == 'column_sub_third_half' || $footer_row_2 == 'column_half_sub_third' || $footer_row_2 == 'column_5_2_2_3') {
                        $footer_row_2 = '4';
                        for ($i=1; $i <= $footer_row_2 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 2 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_2_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_2 == 'column_2_10') {
                        $footer_row_2 = '2';
                        for ($i=1; $i <= $footer_row_2 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 2 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_2_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 2 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }
                }
            break;
        }
    }



    // FOOTER ROW 3
    if (isset($evdev_redux['mt_footer_row_3'])) {
        switch ($evdev_redux['mt_footer_row_3']) {
            case '0':
                break;
            case '1':
                if (isset($evdev_redux['mt_footer_row_3_layout'])) {
                    $footer_row_3 = $evdev_redux['mt_footer_row_3_layout'];
                    $nr3 = array("1", "2", "3", "4", "5", "6");
                    if (in_array($footer_row_3, $nr3)) {
                        for ($i=1; $i <= $footer_row_3 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev').esc_html($i),
                                'id'            => 'footer_row_3_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_3 == 'column_half_sub_half' || $footer_row_3 == 'column_sub_half_half') {
                        $footer_row_3 = '3';
                        for ($i=1; $i <= $footer_row_3 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 3 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_3_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_3 == 'column_sub_fourth_third' || $footer_row_3 == 'column_third_sub_fourth') {
                        $footer_row_3 = '5';
                        for ($i=1; $i <= $footer_row_3 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 3 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_3_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_3 == 'column_sub_third_half' || $footer_row_3 == 'column_half_sub_third' || $footer_row_3 == 'column_5_2_2_3') {
                        $footer_row_3 = '4';
                        for ($i=1; $i <= $footer_row_3 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 3 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_3_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }elseif ($footer_row_3 == 'column_2_10') {
                        $footer_row_3 = '2';
                        for ($i=1; $i <= $footer_row_3 ; $i++) { 
                            register_sidebar( array(
                                'name'          => esc_html__( 'Footer Row 3 - Sidebar ','evdev').esc_html($i),
                                'id'            => 'footer_row_3_'.esc_html($i),
                                'description'   => esc_html__( 'Footer Row 3 - Sidebar ', 'evdev' ) . esc_html($i),
                                'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                                'after_widget'  => '</aside>',
                                'before_title'  => '<h3 class="widget-title">',
                                'after_title'   => '</h3>',
                            ) );
                        }
                    }
                }
                break;
        }
    }
}
add_action( 'widgets_init', 'evdev_widgets_init' );


/**
||-> Enqueue scripts and styles.
*/
function evdev_scripts() {

    //STYLESHEETS
    wp_enqueue_style( "font-awesome", get_template_directory_uri().'/css/font-awesome.min.css' );
    wp_enqueue_style( "cryptocoins", get_template_directory_uri().'/fonts/cryptocoins.css' );
    wp_enqueue_style( "evdev-responsive", get_template_directory_uri().'/css/responsive.css' );
    wp_enqueue_style( "evdev-media-screens", get_template_directory_uri().'/css/media-screens.css' );
    wp_enqueue_style( "owl-carousel2", get_template_directory_uri().'/css/owl.carousel.min.css' );
    wp_enqueue_style( "animate", get_template_directory_uri().'/css/animate.css' );
    wp_enqueue_style( "evdev-style", get_template_directory_uri().'/css/styles.css' );
    wp_enqueue_style( 'evdev-ev-style', get_stylesheet_uri() );
    wp_enqueue_style( "evdev-blogloops-style", get_template_directory_uri().'/css/styles-module-blogloops.css' );
    wp_enqueue_style( "evdev-navigations-style", get_template_directory_uri().'/css/styles-module-navigations.css' );
    wp_enqueue_style( "evdev-header-style", get_template_directory_uri().'/css/styles-headers.css' );
    wp_enqueue_style( "evdev-footer-style", get_template_directory_uri().'/css/styles-footer.css' );
    wp_enqueue_style( "loaders", get_template_directory_uri().'/css/loaders.css' );
    wp_enqueue_style( "simple-line-icons", get_template_directory_uri().'/css/simple-line-icons.css' );
    wp_enqueue_style( "swipebox", get_template_directory_uri().'/css/swipebox.css' );
    wp_enqueue_style( "js-composer", get_template_directory_uri().'/css/js_composer.css' );
    wp_enqueue_style( "evdev-gutenberg-frontend", get_template_directory_uri().'/css/gutenberg-frontend.css' );

    //SCRIPTS
    wp_enqueue_script( 'modernizr-custom', get_template_directory_uri() . '/js/modernizr.custom.js', array('jquery'), '2.6.2', true );
    wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-form', get_template_directory_uri() . '/js/jquery.form.js', array('jquery'), '3.51.0', true );
    wp_enqueue_script( 'jquery-ketchup', get_template_directory_uri() . '/js/jquery.ketchup.js', array('jquery'), '0.3.1', true );
    wp_enqueue_script( 'jquery-validation', get_template_directory_uri() . '/js/jquery.validation.js', array('jquery'), '1.13.1', true );
    wp_enqueue_script( 'jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'uisearch', get_template_directory_uri() . '/js/uisearch.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-parallax', get_template_directory_uri() . '/js/jquery.parallax.js', array('jquery'), '1.1.3', true );
    wp_enqueue_script( 'jquery.appear', get_template_directory_uri() . '/js/jquery.appear.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery.countTo', get_template_directory_uri() . '/js/jquery.countTo.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'modernizr-viewport', get_template_directory_uri() . '/js/modernizr.viewport.js', array('jquery'), '2.6.2', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.1', true );
    wp_enqueue_script( 'animate', get_template_directory_uri() . '/js/animate.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-countdown', get_template_directory_uri() . '/js/jquery.countdown.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'stickykit', get_template_directory_uri() . '/js/jquery.sticky-kit.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'loaders', get_template_directory_uri() . '/js/loaders.js', array('jquery'), '1.0.0', true );

    wp_enqueue_script( 'swipebox', get_template_directory_uri() . '/js/swipebox.js', array('jquery'), '1.4.4', true );
    wp_enqueue_script( 'select2', get_template_directory_uri() . '/js/select2.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'owl-carousel2', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'evdev-custom-js', get_template_directory_uri() . '/js/evdev-custom.js', array('jquery'), '1.0.0', true );

    wp_enqueue_style( "custom_style", get_template_directory_uri().'/css/custom_style.css' );
    wp_enqueue_style( "custom_style_responsive", get_template_directory_uri().'/css/custom_style_responsive.css' );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'evdev_scripts' );


/**
||-> Enqueue admin css/js
*/
function evdev_enqueue_admin_scripts( $hook ) {
    // JS
    wp_enqueue_script( "evdev-admin-scripts", get_template_directory_uri().'/js/evdev-admin-scripts.js' , array( 'jquery' ) );
    // CSS
    wp_enqueue_style( "evdev-admin-css", get_template_directory_uri().'/css/admin-style.css' );
    wp_enqueue_style( 
        'wp-editor-font', 
        'https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i,800,800i,900,900i' 
    ); 
}
add_action('admin_enqueue_scripts', 'evdev_enqueue_admin_scripts');


/**
||-> Enqueue css to js_composer
*/
add_action( 'vc_base_register_front_css', 'evdev_enqueue_front_css_foreever' );
function evdev_enqueue_front_css_foreever() {
    wp_enqueue_style( 'js-composer-front' );
}


/**
||-> Enqueue css to redux
*/
function evdev_register_fontawesome_to_redux() {
    wp_register_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css', array(), time(), 'all' );  
    wp_enqueue_style( 'font-awesome' );
}
add_action( 'redux/page/redux_demo/enqueue', 'evdev_register_fontawesome_to_redux' );


/**
||-> Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
*/
add_action( 'vc_before_init', 'evdev_vcSetAsTheme' );
function evdev_vcSetAsTheme() {
    vc_set_as_theme( true );
}


/**
||-> Other required parts/files
*/
/* ========= LOAD CUSTOM FUNCTIONS ===================================== */
require_once get_template_directory() . '/inc/custom-functions.php';
require_once get_template_directory() . '/inc/custom-functions.header.php';
require_once get_template_directory() . '/inc/custom-functions.footer.php';
require_once get_template_directory() . '/inc/custom-functions.gutenberg.php';
/* ========= Customizer additions. ===================================== */
require_once get_template_directory() . '/inc/customizer.php';
/* ========= Load Jetpack compatibility file. ===================================== */
require_once get_template_directory() . '/inc/jetpack.php';
/* ========= Include the TGM_Plugin_Activation class. ===================================== */
require_once get_template_directory() . '/inc/tgm/include_plugins.php';
/* ========= LOAD - REDUX - FRAMEWORK ===================================== */
require_once get_template_directory() . '/redux-framework/evdevtheme-config.php';
/* ========= CUSTOM COMMENTS ===================================== */
require_once get_template_directory() . '/inc/custom-comments.php';
/* ========= THEME DEFAULTS ===================================== */
require_once get_template_directory() . '/inc/theme-defaults.php';
/* ========= POST DEFAULT CLASS ===================================== */
require_once get_template_directory() . '/inc/post-formats.php';
/* ========= AD PLACES ===================================== */
require_once get_template_directory() . '/inc/ad-places.php';

/**
||-> add_image_size //Resize images
*/
/* ========= RESIZE IMAGES ===================================== */
add_image_size( 'evdev_related_post_pic500x300', 500, 300, true );
add_image_size( 'evdev_post_pic700x450',         700, 450, true );
add_image_size( 'evdev_post_widget_pic100x100',  100, 100, true );
add_image_size( 'evdev_about_625x415',           625, 415, true );
add_image_size( 'evdev_listing_archive_featured_square',    600, 370, true );
add_image_size( 'evdev_listing_archive_featured',    800, 500, true );
add_image_size( 'evdev_listing_archive_thumbnail',   300, 180, true );
add_image_size( 'evdev_listing_single_featured',     1200, 200, true );
add_image_size( 'evdev_news_shortcode_800x600',     800, 600, true );
add_image_size( 'evdev_news_shortcode_1000x700',     1000, 700, true );
add_image_size( 'evdev_news_shortcode_800x800',     800, 800, true );
add_image_size( 'evdev_news_shortcode_1110x550',     1110, 550, true );
add_image_size( 'evdev_news_shortcode_1300x630',     1300, 630, true );
add_image_size( 'evdev_news_shortcode_1300x700',     1300, 700, true );
add_image_size( 'evdev_post_archived',     800, 900, true );
add_image_size( 'evdev_post_wide_full',     1920, 800, true );
add_image_size( 'evdev_sliderv2',     600, 600, true );
add_image_size( 'evdev_gridv1_1',     1200, 780, true );
add_image_size( 'evdev_gridv1_2',     1200, 320, true );
add_image_size( 'evdev_gridv2',     1200, 375, true );
add_image_size( 'evdev_gridv3',     900, 450, true );
add_image_size( 'evdev_post_wide',     1200, 500, true );
add_image_size( 'evdev_post_square',     700, 550, true );
add_image_size( 'evdev_post_square_big',     1200, 650, true );
add_image_size( 'evdev_map_pins',     150, 150, true );
add_image_size( 'evdev_categories_list',     500, 70, true );

// Blogloop-v2
add_image_size( 'evdev_blog_900x550',           900, 550, true );
add_image_size( 'evdev_blog_1920x460',           1920, 460, true );




/**
||-> LIMIT POST CONTENT
*/
function evdev_excerpt_limit($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if(count($words) > $word_limit) {
        array_pop($words);
    }
    return implode(' ', $words);
}


/**
||-> BREADCRUMBS
*/
function evdev_breadcrumb() {
    
    $delimiter = '';
    $html =  '';

    $name = esc_html__("Home", "evdev");
    $currentBefore = '<li class="active">';
    $currentAfter = '</li>';

        if (!is_home() && !is_front_page() || is_paged()) {
            global  $post;
            $home = esc_url(home_url('/'));
            $html .= '<li><a href="' . esc_url($home) . '">' . esc_attr($name) . '</a></li> ' . esc_attr($delimiter) . '';
        
        if (is_category()) {
            global  $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
                if ($thisCat->parent != 0)
            $html .= (get_category_parents($parentCat, true, '' . esc_attr($delimiter) . ''));
            $html .= $currentBefore . single_cat_title('', false) . $currentAfter;
        }elseif (is_tax()) {
            global  $wp_query;
            $html .= $currentBefore . single_cat_title('', false) . $currentAfter;
        } elseif (is_day()) {
            $html .= '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a></li> ' . esc_attr($delimiter) . '';
            $html .= '<li><a href="' . esc_url(get_month_link(get_the_time('Y')), get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . esc_attr($delimiter) . ' ';
            $html .= $currentBefore . get_the_time('d') . $currentAfter;
        } elseif (is_month()) {
            $html .= '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a></li> ' . esc_attr($delimiter) . '';
            $html .= $currentBefore . get_the_time('F') . $currentAfter;
        } elseif (is_year()) {
            $html .= $currentBefore . get_the_time('Y') . $currentAfter;
        } elseif (is_attachment()) {
            $html .= $currentBefore;
            $html .= get_the_title();
            $html .= $currentAfter;
        } elseif (class_exists( 'WooCommerce' ) && is_shop()) {
            $html .= $currentBefore;
            $html .= esc_html__('Shop','evdev');
            $html .= $currentAfter;
        }elseif (class_exists( 'WooCommerce' ) && is_product()) {

            global  $post;
            $cat = get_the_terms( $post->ID, 'product_cat' );
            foreach ($cat as $categoria) {
                if ($categoria) {
                    if($categoria->parent == 0){

                        // Get the ID of a given category
                        $category_id = get_cat_ID( $categoria->name );

                        // Get the URL of this category
                        $category_link = get_category_link( $category_id );

                        $html .= '<li><a href="'.esc_url('#').'">' . esc_attr($categoria->name) . '</a></li>';
                        $html .= esc_url($category_link);
                    }
                }
            }

            $html .= $currentBefore;
            $html .= get_the_title();
            $html .= $currentAfter;

        } elseif (is_single()) {
            if (get_the_category()) {
                $cat = get_the_category();
                $cat = $cat[0];
                $html .= '<li>' . get_category_parents($cat, true, ' ' . esc_attr($delimiter) . '') . '</li>';
            }
            $html .= $currentBefore;
            $html .= get_the_title();
            $html .= $currentAfter;
        } elseif (is_page() && !$post->post_parent) {
            $html .= $currentBefore;
            $html .= get_the_title();
            $html .= $currentAfter;
        } elseif (is_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<li><a href="' . esc_url(get_permalink($page->ID)) . '">' . get_the_title($page->ID) . '</a></li>';
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb)
                $html .= $crumb . ' ' . esc_attr($delimiter) . ' ';
            $html .= $currentBefore;
            $html .= get_the_title();
            $html .= $currentAfter;
        } elseif (is_search()) {
            $html .= $currentBefore . get_search_query() . $currentAfter;
        } elseif (is_tag()) {
            $html .= $currentBefore . single_tag_title( '', false ) . $currentAfter;
        } elseif (is_author()) {
            global  $author;
            $userdata = get_userdata($author);
            $html .= $currentBefore . $userdata->display_name . $currentAfter;
        } elseif (is_404()) {
            $html .= $currentBefore . esc_html__('404 Not Found','evdev') . $currentAfter;
        }
        if (get_query_var('paged')) {
            if (is_home() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                $html .= $currentBefore;
            if (is_home() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                $html .= $currentAfter;
        }
    }

    return $html;
}


// CUSTOM WIDGET
register_sidebar( array(
'name' => 'Link',
'id' => 'link-menu',
'description' => 'Link menu',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Support',
'id' => 'support-menu',
'description' => 'support menu',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Product',
'id' => 'product-menu',
'description' => 'product menu',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Footer details',
'id' => 'footer_details',
'description' => 'Footer details (Logo, Description or social media)',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
/**
||-> SEARCH FOR POSTS ONLY
*/
if (!is_admin()) {
	function evdev_search_filter($query) {
	    if ($query->is_search && !isset($_GET['post_type'])) {
	        if ( !function_exists('evdevtheme_framework')) {
	            $query->set('post_type', 'post');
	        }else{
                if(isset($_GET['post_type']) && $_GET['post_type']=='mt_listing') {
	               $query->set('post_type', 'mt_listing');
                }else{
                    $query->set('post_type', 'post');
                }
	        }
	    }
	    return $query;
	}
	add_filter('pre_get_posts','evdev_search_filter');
}


/**
||-> FUNCTION: ADD EDITOR STYLE
*/
function evdev_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'evdev_add_editor_styles' );

/**
 * Modify image width theme support.
 */
function evdev_iconic_modify_theme_support() {
    $theme_support = get_theme_support( 'woocommerce' );
    $theme_support = is_array( $theme_support ) ? $theme_support[0] : array();

    $theme_support['single_image_width'] = 1000;
    $theme_support['thumbnail_image_width'] = 1000;

    remove_theme_support( 'woocommerce' );
    add_theme_support( 'woocommerce', $theme_support );
}

add_action( 'after_setup_theme', 'evdev_iconic_modify_theme_support', 10 );

/**
||-> REMOVE PLUGINS NOTIFICATIONS and NOTICES
*/
// |---> REVOLUTION SLIDER
if(function_exists( 'set_revslider_as_theme' )){
    add_action( 'init', 'evdev_disable_revslider_update_notices' );
    function evdev_disable_revslider_update_notices() {
        set_revslider_as_theme();
    }
}



/**
 * Show product weight on archive pages
 */
if (!function_exists('evdev_woocommerce_show_weights_on_archive')) {
	add_action( 'woocommerce_after_shop_loop_item', 'evdev_woocommerce_show_weights_on_archive', 9 );
	function evdev_woocommerce_show_weights_on_archive() {

	    global $product;
	    $weight = $product->get_weight();

	    if ( $product->has_weight() ) {
	        echo '<div class="product-meta"><span class="product-meta-label">'.esc_attr__('Weight: ', 'evdev').'</span>' . esc_html($weight) . get_option('woocommerce_weight_unit') . '</div>';
	    }
	}
}

/**
* Related post number
 */
add_filter( 'woocommerce_output_related_products_args', 'evdev_change_number_related_products', 9999 );
 
function evdev_change_number_related_products( $args ) {
     $args['posts_per_page'] = 2; // # of related products
     $args['columns'] = 1; // # of columns per row
     $args['row'] = 2; // # of columns per row
     return $args;
}

function testimonial() {
    // set up product labels
    $labels = array(
        'name' => 'Testimonial',
        'singular_name' => 'Testimonial',
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New Testimonial',
        'edit_item' => 'Edit Testimonial',
        'new_item' => 'New Testimonial',
        'all_items' => 'All Testimonials',
        'view_item' => 'View Testimonials',
        'search_items' => 'Search Testimonials',
        'not_found' =>  'No Testimonials Found',
        'not_found_in_trash' => 'No Testimonials found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Testimonials',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'testimonial'),
        'query_var' => true,
        'menu_icon' => 'dashicons-thumbs-up',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'testimonial', $args );
}
add_action( 'init', 'testimonial' );

// Register Custom Taxonomy for solutions
function testimonial_category() {
        $labels = array(
            'name'                       => _x( 'Testimonial Categories ', 'Taxonomy General Name', 'text_domain' ),
            'singular_name'              => _x( 'Testimonial Categories', 'Taxonomy Singular Name', 'text_domain' ),
            'menu_name'                  => __( 'Testimonial Categories', 'text_domain' ),
            'all_items'                  => __( 'All Categories', 'text_domain' ),
            'parent_item'                => __( 'Parent Item', 'text_domain' ),
            'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
            'new_item_name'              => __( 'New Testimonial Categories', 'text_domain' ),
            'add_new_item'               => __( 'Add Testimonial Categories', 'text_domain' ),
            'edit_item'                  => __( 'Edit Testimonial Categories', 'text_domain' ),
            'update_item'                => __( 'Update Testimonial Categories', 'text_domain' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
            'search_items'               => __( 'Search Categories', 'text_domain' ),
            'add_or_remove_items'        => __( 'Add or remove Testimonial Categories', 'text_domain' ),
            'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
            'not_found'                  => __( 'Not Found', 'text_domain' ),
        );

        $args = array(
            'label'             => 'Testimonials Category',
            'labels'            => $labels,
            'hierarchical'      => true,
            'public'            => true,
            'show_ui'           => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => true,
            'query_var'         => true
        );
    register_taxonomy( 'testimonialcat', 'testimonial', $args );
}

add_action( 'init', 'testimonial_category', 0 );

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 12;
  return $cols;
}
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

add_filter( 'woocommerce_output_related_products_args', 'tijuana_change_number_related_products', 9999 );
 
function tijuana_change_number_related_products( $args ) {
     $args['posts_per_page'] = 2; // # of related products
     $args['columns'] = 2; // # of columns per row
     $args['row'] = 2; // # of columns per row
     return $args;
}

add_action( 'woocommerce_after_shop_loop_item', 'bbloomer_custom_action', 15 );
function bbloomer_custom_action() {
    global $product;
    $product = wc_get_product( get_the_ID() );
    $regular_price = $product->get_regular_price();
    $sale_price = $product->get_sale_price();
    if($sale_price){
        $price = number_format($sale_price, 2, '.', '');
    }else{
        $price = number_format($regular_price, 2, '.', '');
    }
    $current_currency = get_option('woocommerce_currency');
    $curreny_symbol = get_woocommerce_currency_symbol($current_currency);
    $final_price = $curreny_symbol.$price;

    echo '<div class="product-buttons"><p class="overlay-price">'.$final_price.'</p><div class="buy-now-button"><a href="'.get_the_permalink().'">Buy Now</a></div></div>';
}

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 15 );

function buy_now_submit_form() {
 ?>
  <script>
      jQuery(document).ready(function(){
          // listen if someone clicks 'Buy Now' button
          jQuery('#buy_now_button').click(function(){
              // set value to 1
              jQuery('#is_buy_now').val('1');
              //submit the form
              jQuery('form.cart').submit();
          });
      });
  </script>
 <?php
}
add_action('woocommerce_after_add_to_cart_form', 'buy_now_submit_form');

add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url) {
  if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
     global $woocommerce;
     $redirect_url = wc_get_checkout_url();
  }
  return $redirect_url;
}

add_shortcode( 'active_social_links', 'redux_demo_options_social_links' );
function redux_demo_options_social_links() {
    $options = get_option( 'redux_demo' );
    $social_links = array('mt_social_telegram', 'mt_social_fb', 'mt_social_tw', 'mt_social_pinterest', 'mt_social_skype', 'mt_social_instagram', 'mt_social_youtube', 'mt_social_dribbble', 'mt_social_gplus', 'mt_social_linkedin', 'mt_social_deviantart', 'mt_social_digg', 'mt_social_flickr', 'mt_social_stumbleupon', 'mt_social_tumblr', 'mt_social_vimeo');
    $active_links = array();
    foreach( $social_links as $social_link ) {
        if( $options[$social_link] != '' ) {
            if( $social_link == 'mt_social_fb' ) {
                $active_links['facebook'] = $options[$social_link];
            } elseif( $social_link == 'mt_social_tw' ) {
                $active_links['twitter'] = $options[$social_link];
            } else {
                $active_links[end( explode( '_', $social_link ) )] = $options[$social_link];
            }
            
        }
    }    
    $links = '<div class="social_media">';
    $links .= '<div class="social_grid">';
    $links .= '<ul>';
    foreach( $active_links as $key=>$value ) {        
        $links .= '<li><a href="'.$value.'"><i class="fa fa-'.$key.'"></i></a></li>';
    }
    $links .= '</ul>';
    $links .= '</div>';
    $links .= '</div>';
    return $links;
}

add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
add_action( 'wp_ajax_ajaxlogin', 'ajax_login' );

// Check if users input information is valid
function ajax_login() {  

//Nonce is checked, get the POST data and sign user on
$info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

$user_signon = wp_signon( $info, false );
if ( is_wp_error( $user_signon )) {
    echo json_encode( array( 'loggedin'=>false, 'message'=>__( 'Wrong username or password!' )));
} else {
    echo json_encode( array( 'loggedin'=>true, 'message'=>__('Login successful, redirecting...' )));
}

die();
}

/**
 * @snippet       Automatically Update Cart on Quantity Change - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=73470
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.5.1
 */
 
add_action( 'wp_footer', 'bbloomer_cart_refresh_update_qty' ); 
 
function bbloomer_cart_refresh_update_qty() { 
    if (is_cart()) { 
        ?> 
        <script type="text/javascript"> 
            jQuery('div.woocommerce').on('click', 'input.qty', function(){ 
                jQuery("[name='update_cart']").trigger("click"); 
            }); 
        </script> 
        <?php 
    } 
}


/**
 * @snippet Filter the comment content before passing to the database
 */
function pre_process_comment_content( $incoming_comment ) {

    // convert everything in a comment to display literally
    $incoming_comment['comment_content'] = htmlspecialchars($incoming_comment['comment_content']);

    // the one exception is single quotes, which cannot be #039; because WordPress marks it as spam
    $incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] );

    return( $incoming_comment );
}
add_filter( 'preprocess_comment' , 'pre_process_comment_content' );
?>