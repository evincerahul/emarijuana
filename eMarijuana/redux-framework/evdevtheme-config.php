<?php

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_demo";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );


    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Theme Panel', 'evdev' ),
        'page_title'           => esc_html__( 'Theme Panel', 'evdev' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'evdev_redux',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.WordPress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( '', $v );
    } else {
        $args['intro_text'] = '';
    }

    // Add content after the form.
    $args['footer_text'] = '';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'evdev' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'evdev' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'evdev' ),
            'content' => esc_html__( 'This is the tab content, HTML is allowed.', 'evdev' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( 'This is the sidebar content, HTML is allowed.', 'evdev' );
    Redux::setHelpSidebar( $opt_name, $content );
    /*
     * <--- END HELP TABS
     */




    function evdev_get_page_by_post_name($post_name, $output = OBJECT, $post_type = 'post' ){
        global $wpdb;
        $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $post_name, $post_type ) );

        if ( $page ) return get_post( $page, $output );

        return null;
    }
    add_action('init','evdev_get_page_by_post_name');

    /*
     *
     * ---> START SECTIONS
     *
     */


    include_once(get_template_directory(). '/redux-framework/evdevtheme-config.arrays.php');
    include_once(get_template_directory(). '/redux-framework/evdevtheme-config.responsive.php');
    /**
    ||-> SECTION: General Settings
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'General Settings', 'evdev' ),
        'id'    => 'mt_general',
        'icon'  => 'el el-icon-wrench'
    ));
    // GENERAL SETTINGS
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Settings', 'evdev' ),
        'id'         => 'mt_general_settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_breadcrumbs',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Background %2$s', 'evdev' ),'<h3>','</h3>')

            ),
            array(
                'id'       => 'mt_body_global_bg',
                'type'     => 'color',
                'title'    => esc_html__( 'Body Global Background', 'evdev' ),
                'subtitle' => esc_html__( 'Default: #fff', 'evdev' ),
                'default'  => '#ffffff',
            ),
        ),
    ));
    // Back to Top
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Back to Top Button', 'evdev' ),
        'id'         => 'mt_general_back_to_top',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'mt_backtotop_status',
                'type'     => 'switch', 
                'title'    => esc_html__('Back to Top Button Status', 'evdev'),
                'subtitle' => esc_html__('Enable or disable "Back to Top Button"', 'evdev'),
                'default'  => true,
            ),
            array(
                'id'       => 'mt_backtotop_bg_color',
                'type'     => 'color',
                'title'    => esc_html__('Back to Top Button Backgrond', 'evdev'), 
                'subtitle' => esc_html__('Default: Inherit from Predefined Skin', 'evdev'),
                'validate' => 'color',
                'default' => '#151515',
            ),
            array(
                'id'       => 'mt_backtotop_bg_color_hover',
                'type'     => 'color',
                'title'    => esc_html__('Back to Top Button Backgrond - Hover', 'evdev'), 
                'subtitle' => esc_html__('Default: Inherit from Predefined Skin', 'evdev'),
                'validate' => 'color',
                'default' => '#509e46',
            ),
            array(
                'id'       => 'mt_backtotop_text_color',
                'type'     => 'color',
                'title'    => esc_html__('Back to Top Button Icon Color', 'evdev'), 
                'subtitle' => esc_html__('Default: Inherit from Predefined Skin', 'evdev'),
                'validate' => 'color',
                'default' => '#fff',
            ),
            array(
                'id'       => 'mt_backtotop_text_color_hover',
                'type'     => 'color',
                'title'    => esc_html__('Back to Top Button Icon Color - Hover', 'evdev'), 
                'subtitle' => esc_html__('Default: Inherit from Predefined Skin', 'evdev'),
                'validate' => 'color',
                'default' => '#fff',
            ),

        ),
    ));

    // GENERAL SETTINGS
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Page Preloader', 'evdev' ),
        'id' => 'mt_general_preloader',
        'subsection' => true,
        'fields' => array(
            array(
                'id'   => 'mt_divider_preloader_status',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Page Preloader Status %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_preloader_status',
                'type'     => 'switch', 
                'title'    => esc_html__('Enable Page Preloader', 'evdev'),
                'subtitle' => esc_html__('Enable or disable page preloader', 'evdev'),
                'default'  => false,
            )
        ),
    ));
    // SIDEBARS
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Sidebars', 'evdev' ),
        'id'         => 'mt_general_sidebars',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_sidebars',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Generate Infinite Number of Sidebars %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_dynamic_sidebars',
                'type'     => 'multi_text',
                'title'    => esc_html__( 'Sidebars', 'evdev' ),
                'subtitle' => esc_html__( 'Use the "Add More" button to create unlimited sidebars.', 'evdev' ),
                'add_text' => esc_html__( 'Add one more Sidebar', 'evdev' ),
                'options'   => array(
                    'Burger Sidebar',
                    'Sidebar 2'
                ),
            ),
        ),
    ));

    /**
    ||-> SECTION: Styling Settings
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Styling Settings', 'evdev' ),
        'id'    => 'mt_styling',
        'icon'  => 'el el-icon-magic'
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Global Fonts', 'evdev' ),
        'id'         => 'mt_styling_global_fonts',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_googlefonts',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Import Infinite Google Fonts %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_google_fonts_select',
                'type'     => 'select',
                'multi'    => true,
                'title'    => esc_html__('Import Google Font Globally', 'evdev'), 
                'subtitle' => esc_html__('Select one or multiple fonts', 'evdev'),
                'desc'     => esc_html__('Importing fonts made easy', 'evdev'),
                'options'  => $google_fonts_list,
                'default'  => array(
                    'Open Sans:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
                    'Libre+Baskerville:regular,italic,700,latin-ext,latin',
                ),
            ),
        ),
    ));
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Skin color', 'evdev' ),
        'id'         => 'mt_styling_skin_color',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_predefined_skin',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Select a Predefined Skin %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_predefined_skin',
                'type'     => 'palette',
                'title'    => esc_html__( 'Predefined Skin', 'evdev' ),
                'default'  => 'skin_green2',
                'palettes' => array(
                    'skin_blue'  => array(
                        '#3498db',
                        '#2980b9',
                        '#454646',
                    ),
                    'skin_blue2'  => array(
                        '#483ca8',
                        '#3e3492',
                        '#454646',
                    ),
                    'skin_black'  => array(
                        '#222222',
                        '#4f4f4f',
                        '#454646',
                    ),
                    'skin_black_blue'  => array(
                        '#374b9f',
                        '#2d2d2d',
                        '#454646',
                    ),
                    'skin_turquoise'  => array(
                        '#1abc9c',
                        '#16a085',
                        '#454646',
                    ),
                    'skin_green'  => array(
                        '#2ecc71',
                        '#27ae60',
                        '#454646',
                    ),
                    'skin_purple'  => array(
                        '#9b59b6',
                        '#8e44ad',
                        '#454646',
                    ),
                    'skin_yellow'  => array(
                        '#f1c40f',
                        '#f39c12',
                        '#454646',
                    ),
                    'skin_orange'  => array(
                        '#e67e22',
                        '#d35400',
                        '#454646',
                    ),
                    'skin_red'  => array(
                        '#e74c3c',
                        '#c0392b',
                        '#454646',
                    ),
                    'skin_gray'  => array(
                        '#95a5a6',
                        '#7f8c8d',
                        '#454646',
                    ),
                    'skin_yellow2'  => array(
                        '#ffd600',
                        '#e5c000',
                        '#454646',
                    ),
                    'skin_green2'  => array(
                        '#151515',
                        '#509e46',
                        '#808080',
                    ),
                )
            ),
            array(
                'id'   => 'mt_divider_links',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Links Colors(Regular, Hover, Active/Visited) %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_global_link_styling',
                'type'     => 'link_color',
                'title'    => esc_html__('Links Color Option', 'evdev'),
                'subtitle' => esc_html__('Only color validation can be done on this field type(Default Regular: #151515; Default Hover: #509e46; Default Active: #151515;)', 'evdev'),
                'default'  => array(
                    'regular'  => '#151515', // blue
                    'hover'    => '#509e46', // blue-x3
                    'active'   => '#151515',  // blue-x3
                    'visited'  => '#151515',  // blue-x3
                )
            ),
            array(
                'id'   => 'mt_divider_main_colors',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Main Colors & Backgrounds %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_style_main_texts_color',
                'type'     => 'color',
                'title'    => esc_html__('Main texts color', 'evdev'), 
                'subtitle' => esc_html__('Default: #151515', 'evdev'),
                'default'  => '#151515',
                'validate' => 'color',
            ),
            array(
                'id'       => 'mt_style_main_backgrounds_color',
                'type'     => 'color',
                'title'    => esc_html__('Main backgrounds color', 'evdev'), 
                'subtitle' => esc_html__('Default: #ffffff', 'evdev'),
                'default'  => '#ffffff',
                'validate' => 'color',
            ),
            array(
                'id'       => 'mt_style_main_backgrounds_color_hover',
                'type'     => 'color',
                'title'    => esc_html__('Main backgrounds color (hover)', 'evdev'), 
                'subtitle' => esc_html__('Default: #151515', 'evdev'),
                'default'  => '#151515',
                'validate' => 'color',
            ),
            array(
                'id'       => 'mt_style_semi_opacity_backgrounds',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Semitransparent blocks background', 'evdev' ),
                'subtitle' => esc_html__( 'Default: rgba(21, 21, 21, 0.3)', 'evdev' ),
                'default'  => array(
                    'color' => '#151515',
                    'alpha' => '.3'
                ),
                'output' => array(
                    'background-color' => '.fixed-sidebar-menu',
                ),
                'mode'     => 'background'
            ),
            array(
                'id'   => 'mt_divider_text_selection',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Text Selection Color & Background %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_text_selection_color',
                'type'     => 'color',
                'title'    => esc_html__('Text selection color', 'evdev'), 
                'subtitle' => esc_html__('Default: #ffffff', 'evdev'),
                'default'  => '#ffffff',
                'validate' => 'color',
            ),
            array(
                'id'       => 'mt_text_selection_background_color',
                'type'     => 'color',
                'title'    => esc_html__('Text selection background color', 'evdev'), 
                'subtitle' => esc_html__('Default: #509e46', 'evdev'),
                'default'  => '#509e46',
                'validate' => 'color',
            )
        ),
    ));

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Nav Menu', 'evdev' ),
        'id'         => 'mt_styling_nav_menu',
        'subsection' => true,
        'fields'     => array(

            array(
                'id'   => 'mt_divider_nav_menu_layout',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Nav Menu Hover / Layout %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_nav_hover_variant',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => esc_html__( 'Select Navigation Hover / Layout', 'evdev' ),
                'options'  => array(
                    'navstyle-v1' => array(
                        'alt' => esc_html__('Navstyle #1', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_01.png'
                    ),
                    'navstyle-v2' => array(
                        'alt' => esc_html__('Navstyle #2', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_02.png'
                    ),
                    'navstyle-v3' => array(
                        'alt' => esc_html__('Navstyle #3', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_03.png'
                    ),
                    'navstyle-v4' => array(
                        'alt' => esc_html__('Navstyle #4', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_04.png'
                    ),
                    'navstyle-v5' => array(
                        'alt' => esc_html__('Navstyle #5', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_05.png'
                    ),
                    'navstyle-v6' => array(
                        'alt' => esc_html__('Navstyle #6', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_06.png'
                    ),
                    'navstyle-v7' => array(
                        'alt' => esc_html__('Navstyle #7', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_07.png'
                    ),
                    'navstyle-v8' => array(
                        'alt' => esc_html__('Navstyle #8', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/navhovers/navhover_08.png'
                    ),
                ),
                'default'  => 'navstyle-v1'
            ),


            array(
                'id'   => 'mt_divider_nav_menu',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Menus Styling %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_nav_menu_color',
                'type'     => 'color',
                'title'    => esc_html__('Nav Menu Text Color', 'evdev'), 
                'subtitle' => esc_html__('Default: #909090', 'evdev'),
                'default'  => '#909090',
                'validate' => 'color',
                'output' => array(
                    'color' => '#navbar .menu-item > a,
                                .navbar-nav .search_products a,
                                .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus,
                                .navbar-default .navbar-nav > li > a',
                )
            ),
            array(
                'id'       => 'mt_nav_menu_hover_color',
                'type'     => 'color',
                'title'    => esc_html__('Nav Menu Hover Text Color', 'evdev'), 
                'subtitle' => esc_html__('Default: Inherit from Predefined Skin', 'evdev'),
                'default'  => '#151515',
                'validate' => 'color',
                'output' => array(
                    'color' => '#navbar .menu-item.selected > a, #navbar .menu-item:hover > a, #navbar .current-menu-item > a, .navstyle-v1.header3 #navbar .menu > .menu-item:hover > a',
                )
            ),
            array(
                'id'   => 'mt_divider_nav_submenu',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Submenus Styling %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_nav_submenu_background',
                'type'     => 'color',
                'title'    => esc_html__('Nav Submenu Background Color', 'evdev'), 
                'subtitle' => esc_html__('Default: #151515', 'evdev'),
                'default'  => '#151515',
                'validate' => 'color',
                'output' => array(
                    'background-color' => '#navbar .sub-menu, .navbar ul li ul.sub-menu',
                    'border-bottom-color' => '#navbar .sub-menu::before',
                )
            ),
            array(
                'id'       => 'mt_nav_submenu_color',
                'type'     => 'color',
                'title'    => esc_html__('Nav Submenu Text Color', 'evdev'), 
                'subtitle' => esc_html__('Default: #fff', 'evdev'),
                'default'  => '#fff',
                'validate' => 'color',
                'output' => array(
                    'color' => '#navbar ul.sub-menu li a',
                )
            ),
            array(
                'id'       => 'mt_nav_submenu_hover_background_color',
                'type'     => 'color',
                'title'    => esc_html__('Nav Submenu Hover Background Color', 'evdev'), 
                'subtitle' => esc_html__('Default: transparent', 'evdev'),
                'default'  => 'transparent',
                'validate' => 'color',
                'output' => array(
                    'background-color' => '#navbar ul.sub-menu li a:hover',
                )
            ),
            array(
                'id'       => 'mt_nav_submenu_hover_text_color',
                'type'     => 'color',
                'title'    => esc_html__('Nav Submenu Hover Text Color', 'evdev'), 
                'subtitle' => esc_html__('Default: #fff', 'evdev'),
                'default'  => '#fff',
                'validate' => 'color',
                'output' => array(
                    'color' => 'body #navbar ul.sub-menu li a:hover, body #navbar ul.sub-menu li:hover a',
                )
            ),
            array(
                'id'   => 'mt_divider_nav_button',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Request Button %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_request_button_text',
                'type'     => 'text',           
                'title'    => esc_html__('Header Request Button', 'evdev') ,
                'subtitle' => esc_html__('Select title for the Header Request Button.', 'evdev'),
                'desc'     => esc_html__('For example: REQUEST AN APPOINTMENT', 'evdev'),
                'default'  => 'REQUEST AN APPOINTMENT',
            ),
            array(
                'id'       => 'mt_request_button',
                'type'     => 'text',           
                'title'    => esc_html__('Header Request Button URL', 'evdev') ,
                'subtitle' => esc_html__('Select Page for the Header Request Button URL.', 'evdev'),
                'desc'     => esc_html__('For example: http://evdev.evdevtheme.com/appointments/', 'evdev'),
                'validate' => 'url',
                'default'  => 'http://evdev.evdevtheme.com/appointments/',
            ),

        ),
    ));

    /**
    ||-> SECTION: Header Settings
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Header Settings', 'evdev' ),
        'id'    => 'mt_header',
        'icon'  => 'el el-icon-arrow-up'
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header - General', 'evdev' ),
        'id'         => 'mt_header_general',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_generalheader',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Global Header Options %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_header_layout',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => esc_html__( 'Select Header layout', 'evdev' ),
                'options'  => array(
                    'header1' => array(
                        'alt' => esc_html__('Header #1', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/headers/1.png'
                    ),
                ),
                'default'  => 'header1'
            ),
            array(         
                'id'       => 'mt_header_main_background',
                'type'     => 'background',
                'title'    => esc_html__('Header (main-header) - background', 'evdev'),
                'subtitle' => esc_html__('Default color: #ffffff', 'evdev'),
                'output'      => array('.navbar-default'),
                'default'  => array(
                    'background-color' => '#ffffff',
                )
            ),

            array(
                'id'       => 'mt_is_nav_sticky',
                'type'     => 'switch', 
                'title'    => esc_html__('Sticky Navigation Menu?', 'evdev'),
                'subtitle' => esc_html__('Enable or disable "sticky positioned navigation menu".', 'evdev'),
                'default'  => false,
                'on'       => esc_html__( 'Enabled', 'evdev' ),
                'off'      => esc_html__( 'Disabled', 'evdev' )
            ),
            array(
                'id'   => 'mt_divider_header_stat',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Search Icon Settings(from header) %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_header_is_search',
                'type'     => 'switch', 
                'title'    => esc_html__('Search Icon Status', 'evdev'),
                'subtitle' => esc_html__('Enable or Disable Search Icon".', 'evdev'),
                'default'  => false,
                'on'       => esc_html__( 'Enabled', 'evdev' ),
                'off'      => esc_html__( 'Disabled', 'evdev' )
            ),
            array(
                'id'       => 'mt_header_is_search_custom_styling',
                'type'     => 'switch', 
                'title'    => esc_html__('Search Icon - Custom Styling?', 'evdev'),
                'subtitle' => esc_html__('Enable or Disable Custom Styling for Search Icon".', 'evdev'),
                'default'  => false,
                'on'       => esc_html__( 'Yes - Add Custom Colors', 'evdev' ),
                'off'      => esc_html__( 'No - Keep Predefined Colors', 'evdev' )
            ),
            array(
                'id'       => 'mt_header_search_color',
                'type'     => 'color',
                'title'    => esc_html__('Search Icon Color', 'evdev'), 
                'default'  => '#ffffff',
                'validate' => 'color',
                'required' => array( 'mt_header_is_search_custom_styling', '=', true ),
            ),
            array(
                'id'       => 'mt_header_search_color_hover',
                'type'     => 'color',
                'title'    => esc_html__('Search Icon Color - Hover', 'evdev'), 
                'default'  => '#ffd600',
                'validate' => 'color',
                'required' => array( 'mt_header_is_search_custom_styling', '=', true ),
            )
        ),
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo &amp; Favicon', 'evdev' ),
        'id'         => 'mt_header_logo',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_logo',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Logo Settings %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id' => 'mt_logo',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Logo image', 'evdev'),
                'compiler' => 'true',
                'default' => array('url' => get_template_directory_uri().'/images/logo.png'),
            ),
            array(
                'id'        => 'mt_logo_max_width',
                'type'      => 'slider',
                'title'     => esc_html__('Logo Max Width', 'evdev'),
                'subtitle'  => esc_html__('Use the slider to increase/decrease max size of the logo.', 'evdev'),
                'desc'      => esc_html__('Min: 5px, max: 1000px, step: 5px, default value: 340px', 'evdev'),
                "default"   => 200,
                "min"       => 5,
                "step"      => 5,
                "max"       => 1000,
                'display_value' => 'label'
            ),
            array(
                'id'   => 'mt_divider_favicon',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Favicon Settings %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id' => 'mt_favicon',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Favicon url', 'evdev'),
                'compiler' => 'true',
                'subtitle' => esc_html__('Use the upload button to import media.', 'evdev'),
                'default' => array('url' => get_template_directory_uri().'/images/favicon.png'),
            )
        ),
    ) );

    /**

    ||-> SECTION: Footer Settings
    
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Footer Settings', 'evdev' ),
        'id'    => 'mt_footer',
        'icon'  => 'el el-icon-arrow-down'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer General', 'evdev' ),
        'id'         => 'mt_footer_general',
        'subsection' => true,
        'fields'     => array(
            array(         
                'id'       => 'mt_footer_general_background',
                'type'     => 'background',
                'title'    => esc_html__('Footer - background', 'evdev'),
                'subtitle' => esc_html__('Footer background with image or color.', 'evdev'),
                'output'      => array('footer'),
            ),
        ),
    ));


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Top Rows', 'evdev' ),
        'id'         => 'mt_footer_top',
        'subsection' => true,
        'fields'     => array(
            array(         
                'id'       => 'mt_footer_top_background',
                'type'     => 'background',
                'title'    => esc_html__('Footer (top) - background', 'evdev'),
                'subtitle' => esc_html__('Footer background with image or color.', 'evdev'),
                'output'      => array('footer .footer-top'),
                'default'  => array(
                    'background-color' => '#151515',
                )
            ),
            array(
                'id'        => 'mt_footer_top_texts_color',
                'type'      => 'color_rgba',
                'title'     => esc_html__( 'Footer Top Text Color', 'evdev' ),
                'subtitle'  => esc_html__( 'Set color and alpha channel', 'evdev' ),
                'desc'      => esc_html__( 'Set color and alpha channel for footer texts (Especially for widget titles)', 'evdev' ),
                'output'    => array('color' => 'footer .footer-top h1.widget-title, footer .footer-top h3.widget-title, footer .footer-top h3.widget-title a, footer .footer-top .widget-title'),
                'default'   => array(
                    'color'     => '#ffffff',
                    'alpha'     => 1
                ),
                'options'       => array(
                    'show_input'                => true,
                    'show_initial'              => true,
                    'show_alpha'                => true,
                    'show_palette'              => true,
                    'show_palette_only'         => false,
                    'show_selection_palette'    => true,
                    'max_palette_size'          => 10,
                    'allow_empty'               => true,
                    'clickout_fires_change'     => false,
                    'choose_text'               => 'Choose',
                    'cancel_text'               => 'Cancel',
                    'show_buttons'              => true,
                    'use_extended_classes'      => true,
                    'palette'                   => null,  // show default
                    'input_text'                => 'Select Color'
                ),                        
            ),
        ),
    ));



    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer Bottom Bar', 'evdev' ),
        'id'         => 'mt_footer_bottom',
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'mt_footer_text',
                'type' => 'editor',
                'title' => esc_html__('Footer Text', 'evdev'),
                'default' => '<span class="copyright_left">'.esc_html__('Evdev Theme by Evdev. All Rights Reserved','evdev').'</span><span class="copyright_right">'.esc_html__('Elite Author on ThemeForest','evdev').'</span>',
            ),
            array(         
                'id'       => 'mt_footer_bottom_background',
                'type'     => 'background',
                'title'    => esc_html__('Footer (bottom) - background', 'evdev'),
                'subtitle' => esc_html__('Footer background with image or color.', 'evdev'),
                'output'   => array('footer .footer'),
                'default'  => array(
                    'background-color' => '#101010',
                )
            ),
            array(
                'id'        => 'mt_footer_bottom_texts_color',
                'type'      => 'color_rgba',
                'title'     => esc_html__( 'Footer Bottom Text Color', 'evdev' ),
                'subtitle'  => esc_html__( 'Set color and alpha channel', 'evdev' ),
                'desc'      => esc_html__( 'Set color and alpha channel for footer texts (Especially for widget titles)', 'evdev' ),
                'output'    => array('color' => 'footer .footer h1.widget-title, footer .footer h3.widget-title, footer .footer .widget-title, .copyright_left, .copyright_right'),
                'default'   => array(
                    'color'     => '#808080',
                    'alpha'     => 1
                ),
                'options'       => array(
                    'show_input'                => true,
                    'show_initial'              => true,
                    'show_alpha'                => true,
                    'show_palette'              => true,
                    'show_palette_only'         => false,
                    'show_selection_palette'    => true,
                    'max_palette_size'          => 10,
                    'allow_empty'               => true,
                    'clickout_fires_change'     => false,
                    'choose_text'               => 'Choose',
                    'cancel_text'               => 'Cancel',
                    'show_buttons'              => true,
                    'use_extended_classes'      => true,
                    'palette'                   => null,  // show default
                    'input_text'                => 'Select Color'
                ),                        
            ),
        ),
    ));

    /**

    ||-> SECTION: Contact Settings
    
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Contact Settings', 'evdev' ),
        'id'    => 'mt_contact',
        'icon'  => 'el el-icon-map-marker-alt'
    ));
    // GENERAL
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Contact', 'evdev' ),
        'id'         => 'mt_contact_settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'mt_contact_phone',
                'type' => 'text',
                'title' => esc_html__('Phone Number', 'evdev'),
                'subtitle' => esc_html__('Contact phone number displayed on the contact us page.', 'evdev'),
                'default' => '(+40) 74 0920 2288'
            ),
            array(
                'id' => 'mt_contact_email',
                'type' => 'text',
                'title' => esc_html__('Email', 'evdev'),
                'subtitle' => esc_html__('Contact email displayed on the contact us page., additional info is good in here.', 'evdev'),
                'validate' => 'email',
                'msg' => 'custom error message',
                'default' => 'evdev@example.com'
            ),
            array(
                'id' => 'mt_contact_address',
                'type' => 'text',
                'title' => esc_html__('Address', 'evdev'),
                'subtitle' => esc_html__('Enter your contact address', 'evdev'),
                'default' => 'New York 11673 Collins Street'
            )
        ),
    ));
    
    // MAILCHIMP
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Mailchimp', 'evdev' ),
        'id'         => 'mt_contact_mailchimp',
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'mt_mailchimp_apikey',
                'type' => 'text',
                'title' => esc_html__('Mailchimp apiKey', 'evdev'),
                'subtitle' => esc_html__('To enable Mailchimp please type in your apiKey', 'evdev'),
                'default' => ''
            ),
            array(
                'id' => 'mt_mailchimp_listid',
                'type' => 'text',
                'title' => esc_html__('Mailchimp listId', 'evdev'),
                'subtitle' => esc_html__('To enable Mailchimp please type in your listId', 'evdev'),
                'default' => ''
            ),
            array(
                'id' => 'mt_mailchimp_data_center',
                'type' => 'text',
                'title' => esc_html__('Mailchimp form datacenter', 'evdev'),
                'subtitle' => esc_html__('To enable Mailchimp please type in your form datacenter', 'evdev'),
                'default' => ''
            )
        ),
    ));



    /**
    ||-> SECTION: Blog Settings
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Blog Settings', 'evdev' ),
        'id'    => 'mt_blog',
        'icon'  => 'el el-icon-comment'
    ));
    // SIDEBARS
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Blog Archive', 'evdev' ),
        'id'         => 'mt_blog_archive',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_blog_layout',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Blog List Layout %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_blogloop_variant',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => esc_html__( 'Select Blog Loop Design', 'evdev' ),
                'options'  => array(
                    'blogloop-v2' => array(
                        'alt' => esc_html__('Blogloop v2', 'evdev'),
                        'img' => get_template_directory_uri().'/redux-framework/assets/blogloops/blogloop-v2.png'
                    ),
                ),
                'default'  => 'blogloop-v2'
            ),
            array(
                'id'       => 'mt_blog_layout',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => esc_html__( 'Blog List Layout', 'evdev' ),
                'subtitle' => esc_html__( 'Select Blog List layout.', 'evdev' ),
                'options'  => array(
                    'mt_blog_right_sidebar' => array(
                        'alt' => esc_html__('2 Columns - Right sidebar', 'evdev' ),
                        'img' => get_template_directory_uri().'/redux-framework/assets/sidebar-right.jpg'
                    )
                ),
                'default'  => 'mt_blog_right_sidebar'
            ),
            array(
                'id'       => 'mt_blog_layout_sidebar',
                'type'     => 'select',
                'data'     => 'sidebars',
                'title'    => esc_html__( 'Blog List Sidebar', 'evdev' ),
                'subtitle' => esc_html__( 'Select Blog List Sidebar.', 'evdev' ),
                'default'   => 'sidebar-1',
                'required' => array('mt_blog_layout', '!=', 'mt_blog_fullwidth'),
            ),
            array(
                'id'   => 'mt_divider_blog_elements',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Blog List Elements %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id' => 'mt_blog_post_title',
                'type' => 'text',
                'title' => esc_html__('Blog Post Title', 'evdev'),
                'subtitle' => esc_html__('Enter the text you want to display as blog post title.', 'evdev'),
                'default' => 'All Blog Posts'
            )
        ),
    ));

    // SIDEBARS
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Single Post', 'evdev' ),
        'id'         => 'mt_blog_single_pos',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_single_blog_typo',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Single Blog Post Font family %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'          => 'mt_single_post_typography',
                'type'        => 'typography', 
                'title'       => esc_html__('Blog Post Font family', 'evdev'),
                'subtitle'    => esc_html__( 'Default color: #454646; Font-size: 18px; Line-height: 29px;', 'evdev' ),
                'google'      => true, 
                'font-size'   => true,
                'line-height' => true,
                'color'       => true,
                'font-backup' => false,
                'text-align'  => false,
                'letter-spacing'  => false,
                'font-weight'  => true,
                'font-style'  => false,
                'subsets'     => false,
                'units'       =>'px',
                'default'     => array(
                    'color' => '#808080', 
                    'font-size' => '16px', 
                    'line-height' => '25px', 
                    'text-align' => 'left',
                    'font-family' => 'Open Sans', 
                    'google'      => true
                ),
            ),
            array(
                'id'   => 'mt_divider_single_blog_elements',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Other Single Post Elements %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_post_featured_image',
                'type'     => 'switch', 
                'title'    => esc_html__('Single post featured image.', 'evdev'),
                'subtitle' => esc_html__('Show or Hide the featured image from blog post page.".', 'evdev'),
                'default'  => true,
            ),
            array(
                'id'       => 'mt_enable_related_posts',
                'type'     => 'switch', 
                'title'    => esc_html__('Related Posts', 'evdev'),
                'subtitle' => esc_html__('Enable or disable related posts', 'evdev'),
                'default'  => false,
            ),
            array(
                'id'       => 'mt_enable_post_navigation',
                'type'     => 'switch', 
                'title'    => esc_html__('Post Navigation', 'evdev'),
                'subtitle' => esc_html__('Enable or disable post navigation', 'evdev'),
                'default'  => false,
            ),
            array(
                'id'       => 'mt_enable_authorbio',
                'type'     => 'switch', 
                'title'    => esc_html__('About Author', 'evdev'),
                'subtitle' => esc_html__('Enable or disable "About author" section on single post', 'evdev'),
                'default'  => true,
            ),
        ),
    ));
    

    /**
    ||-> SECTION: Social Media Settings
    */
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Social Media Settings', 'evdev' ),
        'id'    => 'mt_social_media',
        'icon'  => 'el el-icon-myspace'
    ));
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social Media', 'evdev' ),
        'id'         => 'mt_social_media_settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_global_social_links',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Global Social Links %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id' => 'mt_social_telegram',
                'type' => 'text',
                'title' => esc_html__('Telegram URL', 'evdev'),
                'subtitle' => esc_html__('Type your Telegram url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_fb',
                'type' => 'text',
                'title' => esc_html__('Facebook URL', 'evdev'),
                'subtitle' => esc_html__('Type your Facebook url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_tw',
                'type' => 'text',
                'title' => esc_html__('Twitter username', 'evdev'),
                'subtitle' => esc_html__('Type your Twitter username.', 'evdev'),
                'default' => ''
            ),
            array(
                'id' => 'mt_social_pinterest',
                'type' => 'text',
                'title' => esc_html__('Pinterest URL', 'evdev'),
                'subtitle' => esc_html__('Type your Pinterest url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_skype',
                'type' => 'text',
                'title' => esc_html__('Skype Name', 'evdev'),
                'subtitle' => esc_html__('Type your Skype username.', 'evdev'),
                'default' => ''
            ),
            array(
                'id' => 'mt_social_instagram',
                'type' => 'text',
                'title' => esc_html__('Instagram URL', 'evdev'),
                'subtitle' => esc_html__('Type your Instagram url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_youtube',
                'type' => 'text',
                'title' => esc_html__('YouTube URL', 'evdev'),
                'subtitle' => esc_html__('Type your YouTube url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_dribbble',
                'type' => 'text',
                'title' => esc_html__('Dribbble URL', 'evdev'),
                'subtitle' => esc_html__('Type your Dribbble url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_gplus',
                'type' => 'text',
                'title' => esc_html__('Google+ URL', 'evdev'),
                'subtitle' => esc_html__('Type your Google+ url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_linkedin',
                'type' => 'text',
                'title' => esc_html__('LinkedIn URL', 'evdev'),
                'subtitle' => esc_html__('Type your LinkedIn url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_deviantart',
                'type' => 'text',
                'title' => esc_html__('Deviant Art URL', 'evdev'),
                'subtitle' => esc_html__('Type your Deviant Art url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_digg',
                'type' => 'text',
                'title' => esc_html__('Digg URL', 'evdev'),
                'subtitle' => esc_html__('Type your Digg url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_flickr',
                'type' => 'text',
                'title' => esc_html__('Flickr URL', 'evdev'),
                'subtitle' => esc_html__('Type your Flickr url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_stumbleupon',
                'type' => 'text',
                'title' => esc_html__('Stumbleupon URL', 'evdev'),
                'subtitle' => esc_html__('Type your Stumbleupon url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_tumblr',
                'type' => 'text',
                'title' => esc_html__('Tumblr URL', 'evdev'),
                'subtitle' => esc_html__('Type your Tumblr url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
            array(
                'id' => 'mt_social_vimeo',
                'type' => 'text',
                'title' => esc_html__('Vimeo URL', 'evdev'),
                'subtitle' => esc_html__('Type your Vimeo url.', 'evdev'),
                'validate' => 'url',
                'default' => ''
            ),
        ),
    ));
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Floating Social Button', 'evdev' ),
        'id'         => 'mt_social_media_settings_fixed_social_btn',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'   => 'mt_divider_global_social_links_footer',
                'type' => 'info',
                'class' => 'mt_divider',
                'desc' => sprintf (esc_html__( '%1$s Floating Social Button %2$s', 'evdev' ),'<h3>','</h3>')
            ),
            array(
                'id'       => 'mt_fixed_social_btn_status',
                'type'     => 'switch', 
                'title'    => esc_html__('Enable Floating Social Button', 'evdev'),
                'subtitle' => esc_html__('Enable or disable Floating Social Button', 'evdev'),
                'default'  => false,
            ),
            array(
                'id'       => 'mt_fixed_social_btn_social_select',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Social Media url to show', 'evdev' ),
                'subtitle' => esc_html__( 'Url/Icon can be set from Social Media tab - on Theme Panel', 'evdev' ),
                'options'  => array(
                    'telegram'      => esc_html__( 'Telegram Link/Icon', 'evdev' ),
                    'facebook'      => esc_html__( 'Facebook Link/Icon', 'evdev' ),
                    'twitter'      => esc_html__( 'Facebook Link/Icon', 'evdev' ),
                    'pinterest'      => esc_html__( 'Pinterest Link/Icon', 'evdev' ),
                    'skype'      => esc_html__( 'Skype Link/Icon', 'evdev' ),
                    'instagram'      => esc_html__( 'Instagram Link/Icon', 'evdev' ),
                    'youtube'      => esc_html__( 'YouTube Link/Icon', 'evdev' ),
                    'dribbble'      => esc_html__( 'Dribbble Link/Icon', 'evdev' ),
                    'googleplus'      => esc_html__( 'Google+ Link/Icon', 'evdev' ),
                    'linkedin'      => esc_html__( 'LinkedIn Link/Icon', 'evdev' ),
                    'deviantart'      => esc_html__( 'LinkedIn Link/Icon', 'evdev' ),
                    'digg'      => esc_html__( 'Digg Link/Icon', 'evdev' ),
                    'flickr'      => esc_html__( 'Flickr Link/Icon', 'evdev' ),
                    'stumbleupon'      => esc_html__( 'Stumbleupon Link/Icon', 'evdev' ),
                    'tumblr'      => esc_html__( 'Tumblr Link/Icon', 'evdev' ),
                    'vimeo'      => esc_html__( 'Vimeo Link/Icon', 'evdev' ),
                ),
                'default'  => 'telegram',
                'required' => array( 'mt_fixed_social_btn_status', '=', '1' ),
            ),
        ),
    ));
    /*
     * <--- END SECTIONS
     */
