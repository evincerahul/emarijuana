<?php
/**
 * The template for displaying the footer.
 *
*/
?>
	



    
    <!-- BEGIN: FLOATING SOCIAL BUTTON -->
    <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
        <?php echo evdev_floating_social_button(); ?>
    <?php } ?>
    <!-- END: FLOATING SOCIAL BUTTON -->

    <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
        <!-- BACK TO TOP BUTTON -->
        <a class="back-to-top evdevtheme-is-visible evdevtheme-fade-out" href="<?php echo esc_url('#0'); ?>">
            <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
        </a>
    <?php } else { ?>
        <?php if (evdev_redux('mt_backtotop_status') == true) { ?>
            <!-- BACK TO TOP BUTTON -->
            <a class="back-to-top evdevtheme-is-visible evdevtheme-fade-out" href="<?php echo esc_url('#0'); ?>">
                <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
            </a>
        <?php } ?>
    <?php } ?>

    <!-- FOOTER -->
    <footer>

        <!-- FOOTER TOP -->
        <div class="row footer-top">
            <div class="container">
            <?php          
                //FOOTER ROW #1
                echo evdev_footer_row1();
                //FOOTER ROW #2
                echo evdev_footer_row2();
                //FOOTER ROW #3
                echo evdev_footer_row3();
             ?>
            </div>
        </div>

        <!-- FOOTER BOTTOM -->
        <div class="footer-div-parent">
            <div class="footer">
                <?php echo do_shortcode( "[mc4wp_form id='126']" ); ?>
                <div class="footer_bottom">
                    <div class="footer_wraper">
                    <div class="container">
                        <div class="footer_boxes">
                        <div class="f_bottom_left footer_logo_description">
                                <?php
                                if(is_active_sidebar('footer_details')){
                                dynamic_sidebar('footer_details');
                                }
                                ?>
                        </div>
                        <?php
                            global $post;
                            $age_option = get_option( 'psag_options' )['psag_options_redirect_url'];
                            if( isset( $age_option ) && $post->post_name != end( explode( '/', $age_option ) ) ) {
                        ?>
                        <div class="f_bottom_right">
                            <div class="footer_link">
                                <?php
                                if(is_active_sidebar('link-menu')){
                                dynamic_sidebar('link-menu');
                                }
                                ?>
                            </div>
                            <div class="footer_link">
                                  <?php
                                if(is_active_sidebar('support-menu')){
                                dynamic_sidebar('support-menu');
                                }
                                ?>
                            </div>
                            <div class="footer_link">
                                 <?php
                                if(is_active_sidebar('product-menu')){
                                dynamic_sidebar('product-menu');
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="container_inner_footer">
                        <div class="row">
                            <div class="col-md-12">
                            	<p class="copyright text-center">
                                    <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
                                        <?php echo wp_kses_post(evdev_redux('mt_footer_text')); ?>
                                    <?php }else{ ?>
                                        <span class="copyright_left"><?php echo esc_html__('Evdev Theme by Evdev. All Rights Reserved', 'evdev'); ?></span>
                                        <span class="copyright_right"><?php echo esc_html__('Elite Author on ThemeForest', 'evdev'); ?></span>
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>


<?php wp_footer(); ?>
</body>
</html>