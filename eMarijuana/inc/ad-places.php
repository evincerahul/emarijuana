<?php 
//Insert ads after second paragraph of single post content.
 
if (function_exists('evdev_extend_redux_tabs')){
    add_filter( 'the_content', 'evdev_insert_post_ads' );
    function evdev_insert_post_ads( $content ) {
         
    $ad_code = '';

        if (evdev_redux('mt_adplace_blog_post')) {
            if(evdev_redux('mt_adplace_blog_post_adsense_code') != '' && evdev_redux('mt_adplace_blog_post') == 'on_adsense'){
                $ad_code = evdev_redux('mt_adplace_blog_post_adsense_code');
            }elseif(evdev_redux('mt_adplace_blog_post') == 'on'){
                $ad_code = '<div class="adplace-blog-post">
                    <a href="'.esc_url(evdev_redux('mt_adplace_blog_post_link')).'" target="_blank">
                        <img src="'.esc_url(evdev_redux('mt_adplace_blog_post_img','url')).'" alt="'.esc_attr__('adPlace','evdev').'" />
                    </a>
                </div>';
            }elseif (evdev_redux('mt_adplace_blog_post') == 'off') {
                $ad_code = '';
            }
        }

        if ( is_single() && ! is_admin() ) {
            return evdev_insert_after_paragraph( $ad_code, 3, $content );
        }
         
        return $content;
    }
      
    // Parent Function that makes the magic happen
    function evdev_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
        $closing_p = '</p>';
        $paragraphs = explode( $closing_p, $content );
        foreach ($paragraphs as $index => $paragraph) {
     
            if ( trim( $paragraph ) ) {
                $paragraphs[$index] .= $closing_p;
            }
     
            if ( $paragraph_id == $index + 1 ) {
                $paragraphs[$index] .= $insertion;
            }
        }
         
        return implode( '', $paragraphs );
    }
}
?>