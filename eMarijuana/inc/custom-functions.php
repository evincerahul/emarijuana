<?php


//GET HEADER TITLE/BREADCRUMBS AREA
function evdev_header_title_breadcrumbs(){

    $css_inline = '';
    $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ),'evdev_breadcrumbs' );
        if ( is_page() ) {
            if (!empty($thumbnail_src[0])) {
                $css_inline = 'background-image:url('.esc_url($thumbnail_src[0]).');';
            }
        }

    $html = '';
    $html .= '<div class="header-title-breadcrumb relative">';
        $html .= '<div class="header-title-breadcrumb-overlay text-center" style="'.$css_inline.'">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                    <div class="breadcrumb-box">';
                                        if (class_exists( 'WooCommerce' ) && is_product()) {
                                            $html .= '<h1>'.esc_html__( 'Shop', 'evdev' ) . get_search_query().'</h1>';
                                        }elseif (class_exists( 'WooCommerce' ) && is_shop()) {
                                            $html .= '<h1>'.esc_html__( 'Shop', 'evdev' ) . get_search_query().'</h1>';
                                        }elseif (is_singular('post')) {
                                            $html .= '<h1>'.esc_html__( 'Blog', 'evdev' ) . get_search_query().'</h1>';
                                        }elseif (is_page()) {
                                            $html .= '<h1>'.get_the_title().'</h1>';
                                        }elseif (is_search()) {
                                            $html .= '<h1>'.esc_html__( 'Search Results for: ', 'evdev' ) . get_search_query().'</h1>';
                                        }elseif (is_category()) {
                                            $html .= '<h1>'.esc_html__( 'Category: ', 'evdev' ).' <span>'.single_cat_title( '', false ).'</span></h1>';
                                        }elseif (is_tag()) {
                                            $html .= '<h1>'.esc_html__( 'Tag Archives: ', 'evdev' ) . single_tag_title( '', false ).'</h1>';
                                        }elseif (is_author() || is_archive()) {
                                            $html .= '<h1>'.get_the_archive_title().'</h1>';
                                        }elseif (is_home()) {
                                            $html .= '<h1>'.esc_html__( 'From the Blog', 'evdev' ).'</h1>';
                                        }else {
                                            $html .= '<h1>'.get_the_title().'</h1>';
                                        }
                                    $html .= '</div>                   
                                </div>
                            </div>
                        </div>
                    </div>';

    $html .= '</div>';
    $html .= '<div class="clearfix"></div>';

    return $html;
}



//GET Social Floating button
if (!function_exists('evdev_floating_social_button')) {
    function evdev_floating_social_button(){

        $html = '';
        $link = '';
        $fa_class = '';

        if (evdev_redux('mt_fixed_social_btn_status') == true) {
            if (evdev_redux('mt_fixed_social_btn_social_select') == 'telegram') {
                $link = evdev_redux('mt_social_telegram');
                $fa_class = 'fa fa-telegram';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'facebook') {
                $link = evdev_redux('mt_social_fb');
                $fa_class = 'fa fa-facebook';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'twitter') {
                $link = evdev_redux('mt_social_tw');
                $fa_class = 'fa fa-twitter';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'googleplus') {
                $link = evdev_redux('mt_social_gplus');
                $fa_class = 'fa fa-google-plus';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'youtube') {
                $link = evdev_redux('mt_social_youtube');
                $fa_class = 'fa fa-youtube-play';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'pinterest') {
                $link = evdev_redux('mt_social_pinterest');
                $fa_class = 'fa fa-pinterest-p';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'pinterest') {
                $link = evdev_redux('mt_social_pinterest');
                $fa_class = 'fa fa-pinterest-p';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'linkedin') {
                $link = evdev_redux('mt_social_linkedin');
                $fa_class = 'fa fa-linkedin';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'skype') {
                $link = evdev_redux('mt_social_skype');
                $fa_class = 'fa fa-skype';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'instagram') {
                $link = evdev_redux('mt_social_instagram');
                $fa_class = 'fa fa-instagram';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'dribbble') {
                $link = evdev_redux('mt_social_dribbble');
                $fa_class = 'fa fa-dribbble';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'deviantart') {
                $link = evdev_redux('mt_social_deviantart');
                $fa_class = 'fa fa-deviantart';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'digg') {
                $link = evdev_redux('mt_social_digg');
                $fa_class = 'fa fa-digg';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'flickr') {
                $link = evdev_redux('mt_social_flickr');
                $fa_class = 'fa fa-flickr';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'stumbleupon') {
                $link = evdev_redux('mt_social_stumbleupon');
                $fa_class = 'fa fa-stumbleupon';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'tumblr') {
                $link = evdev_redux('mt_social_tumblr');
                $fa_class = 'fa fa-tumblr';
            }elseif (evdev_redux('mt_fixed_social_btn_social_select') == 'vimeo') {
                $link = evdev_redux('mt_social_vimeo');
                $fa_class = 'fa fa-vimeo';
            }


            $html .= '<a data-toggle="tooltip" data-placement="top" title="'.esc_attr__('Connect on Telegram','evdev').'" class="floating-social-btn" target="_blank" href="'.esc_url($link).'">';
                $html .= '<i class="'.esc_attr($fa_class).'"></i>';
            $html .= '</a>';
        }

        return $html;
    }
}

if ( function_exists( 'evdevtheme_framework' ) ) {
    function evdev_dfi_ids($postID){
        global  $dynamic_featured_image;
        $featured_images = $dynamic_featured_image->get_featured_images( $postID );
        //Loop through the image to display your image
        if( !is_null($featured_images) ){
            $medias = array();
            foreach($featured_images as $images){
                $attachment_id = $images['attachment_id'];
                $medias[] = $attachment_id;
            }
            $ids = '';
            $len = count($medias);
            $i = 0;
            foreach($medias as $media){
                if ($i == $len - 1) {
                    $ids .= $media;
                }else{
                    $ids .= $media . ',';
                }
                $i++;
            }
        } 
        return $ids;
    }
}


/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'evdev_loop_columns');
if (!function_exists('evdev_loop_columns')) {
    function evdev_loop_columns() {
        return 1; // 1 products per row
    }
}